// L2B

import express from 'express';
import getNormalizeName from './getNormalizeName';

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function(req,res) {

	var fullname = req.query.fullname;
	console.log(fullname);
	
	const response = getNormalizeName(fullname);
	
	return res.send(response, {'Content-Type': 'text/plain'}, 200);

});

app.listen(3000, function() {
});




