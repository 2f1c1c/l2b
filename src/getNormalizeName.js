export default function getNormalizeName(fullname) {
	
	fullname = fullname.trim();
	fullname = fullname.replace(/\s+/g, ' '); 	
	fullname = fullname.toLowerCase();
	var worlds = fullname.split(' ');
 	var requestName;
	const reg = /\_|\d|\//;	
	const worldsCount = worlds.length;
	
	if (reg.test(fullname)) { 
		requestName = "Invalid fullname"; 	 
  } 
  else {
  
  if (fullname.length === 0 ) {
	  requestName = "Invalid fullname";
	}
	else if (worldsCount === 1) {
		var lastName = worlds[0];
		requestName = lastName[0].toUpperCase() + lastName.slice(1);
	}	
	else if (worldsCount === 2) {
		var firstName = worlds[0]; 
		var lastName = worlds[1];
		firstName = firstName.slice(0,1).toUpperCase() + '.';
		lastName = lastName[0].toUpperCase() + lastName.slice(1);
		requestName = lastName + ' ' +  firstName;	
	}
	else if (worldsCount === 3) {
		var firstName = worlds[0];
		var patronymic = worlds[1];
		var lastName = worlds[2];
		firstName = firstName.slice(0,1).toUpperCase() + '.';
		patronymic = patronymic.slice(0,1).toUpperCase() + '.';
		lastName = lastName[0].toUpperCase() + lastName.slice(1);
		requestName = lastName + ' ' + firstName + ' ' + patronymic;
	}
	else if (worldsCount > 3) {
		requestName = "Invalid fullname";
	 }
	 	
	}
	
	return requestName;
	
}
